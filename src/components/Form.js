import React from 'react';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { connect } from 'react-redux';
import * as actions from '../store/actions/text';
import ReactMarkdown from 'react-markdown';


class CustomForm extends React.Component {

  handleChange = e => {
     e.preventDefault();
     this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.TextUpdate(values.text);
      }
    });

  }


  render() {

const { TextArea } = Input;
    const { getFieldDecorator } = this.props.form;
    return (
      <div class='container'>

      <Form onChange={this.handleChange}>
        <Form.Item>
          {getFieldDecorator('text', {
            rules: [{ required: true, message: 'Please input your text here' }],
          })(
            <TextArea value={'werwe'} style={{ marginTop: '5vh', width: '100%', height: '90vh'}}
              placeholder="Text"
            />,
          )}
        </Form.Item>
      </Form>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return{
    TextUpdate: (data) => dispatch(actions.textUpdate(data))
  }
};
const mapStateToProps = (state) => {
  return{
    text: state.text
  }
};
const WrappedForm = Form.create({ name: 'normal_form' })(CustomForm);
export default connect(mapStateToProps, mapDispatchToProps)(WrappedForm);

import React from 'react';
import CustomForm from '../components/Form';
import { connect } from 'react-redux';
import ReactMarkdown from 'react-markdown';

class Wrap extends React.Component {


  render() {

    console.log(this.props.text);
    return (
      <div className='container'>
        <div className='row'>
          <div className='col-md'>
            <CustomForm/>
          </div>
          <div style={{ marginTop: '5vh'}} className='col-md'>
            <ReactMarkdown  source={this.props.text} escapeHtml={false}/>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return{
    text: state.text
  }
};

export default connect(mapStateToProps)(Wrap);

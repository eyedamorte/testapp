import React from 'react';
import 'antd/dist/antd.css';
import * as actions from './store/actions/text';
import Wrap from './containers/Wrap';
import { connect } from 'react-redux';
function App() {
  return (
    <div className="App">
      <Wrap/>
    </div>
  );
}

export default App;


import * as actionTypes from './actionTypes';

export const textUpdate = (data) => {
  return{
    type: actionTypes.TEXT_UPDATE,
    payload:{
      data
    }
  }
}

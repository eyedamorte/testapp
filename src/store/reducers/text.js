import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';


const initialState = {
  text: ''
};


export default (state = initialState, action) => {
  switch(action.type){
    case 'TEXT_UPDATE':
      return{
        text: action.payload.data
      };
    default:
      return state;
  }
}
